$( document ).ready(function() {
   $("#open_menu").click(function () {
       $("#navigation-menu").toggleClass("open");
   });
    $("#close_menu").click(function () {
        $("#navigation-menu").toggleClass("open");
    });
    $("#cart").click(function () {
        $(".cart").toggleClass("open");
        $(".cart-open").toggleClass("open");
    });
    $("#open_faq").click(function () {
        $("#open_faq").toggleClass("active");
        $(".item").toggleClass("open");
        $(".faq").css("margin-bottom:0");
    });
});


$("#myTabs a").click(function (e) {
    e.preventDefault()
    $(this).tab('show')
});

$(".sing-up").click(function () {
    $(".sing-up").css("display", "none");
    $(".profile-head").css("display", "block");
});
$("#copy").click(function () {
    var target = $("#copy")[0];
    target.focus();
    target.setSelectionRange(0, target.value.length);

// copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }

    $('.copy_tooltip').show(200).delay(5000).hide(100);
});
